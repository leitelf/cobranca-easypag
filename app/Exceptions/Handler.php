<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use \Illuminate\Http\Client\ConnectionException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
    ];

    // public function render($request, Exception $exception)
    // {
    //     echo var_dump($exception);
    //     if ($exception instanceof \Illuminate\Http\Client\ConnectionException) {
    //         return \Illuminate\Support\Facades\Redirect::back()->withErrors(['error' => 'Parece que o serviço Easypag não está funcionando no momento. Tente novamente mais tarde.']);
    //     }

    //     return parent::render($request, $exception);
    // }

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (ConnectionException $e, $request) {
            return \Illuminate\Support\Facades\Redirect::back()
            ->withInput()
            ->withErrors(['error' => 'Parece que o serviço Easypag não está funcionando no momento. Tente novamente mais tarde.']);
        });
    }
}
