<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Charge extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice_id',
        'due_date',
        'amount',
        'name',
        'doc',
        'invoice_url'
    ];
}
