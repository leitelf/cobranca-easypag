<?php

namespace App\Http\Controllers;

use App\Models\Charge;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Rules\checkDocRule;

class ChargeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $charges = Charge::latest()->paginate(5);
        return view('charges.index', ['charges' => $charges]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('charges.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $states = [
            'RO',
            'AC',
            'AM',
            'RR',
            'PA',
            'AP',
            'TO',
            'MA',
            'PI',
            'CE',
            'RN',
            'PB',
            'PE',
            'AL',
            'SE',
            'BA',
            'MG',
            'ES',
            'RJ',
            'SP',
            'PR',
            'SC',
            'RS',
            'MS',
            'MT',
            'GO',
            'DF'
        ];

        $validated = $request->validate([
            'dueDate' => 'required|date|after_or_equal:today',
            'email' => 'required|email',
            'phoneNumber' => 'required|digits_between:12,13',
            'docNumber' => [
                'required',
                new checkDocRule()
            ],
            'interest_percentage' => 'nullable|numeric|between:0.01,100.0',
            'cep' => 'required|digits:8',
            'uf' => 'required|in:'.implode(',', $states),
            'area' => 'required',
            'addressLine1' => 'required',
            'streetNumber' => 'required',
            'item_description' =>   'required|min:1',
            'item_description.*' => 'required|string',
            'item_quantity' => 'required|min:1',
            'item_quantity.*' => 'required|integer|min:1',
            'item_price' => 'required|min:1',
            'item_price.*' => 'required|numeric'
        ]);

        $easypag_api = env('EASYPAG_URL');
        $easypag_user = env('EASYPAG_USER');
        $easypag_pass = env('EASYPAG_PASSWORD');

        $itemsCount = count($inputs['item_description']);

        $items = [];
        for($i = 0; $i < $itemsCount; $i++) {
            $item = [
                'description' => $inputs['item_description'][$i],
                'quantity' => (int) $inputs['item_quantity'][$i],
                'price' => (int)(((float) $inputs['item_price'][$i]) * 100.0)
            ];

            array_push($items, $item);
        }

        $reqdata = [
            'dueDate' => date("Y-m-d", strtotime($inputs['dueDate'])),
            'instructionsMsg' => $inputs['instructionsMsg'],
            'interest' => [
                'percentage' => ((float) $inputs['interest_percentage']) / 100.0
            ],
            'customer' => [
                'name' => $inputs['name'],
                'email' => $inputs['email'],
                'phoneNumber' => $inputs['phoneNumber'],
                'docNumber' => $inputs['docNumber'],
                'address' => [
                    'cep' => $inputs['cep'],
                    'uf' => $inputs['uf'],
                    'city' => $inputs['city'],
                    'area' => $inputs['area'],
                    'addressLine1' => $inputs['addressLine1'],
                    'addressLine2' => $inputs['addressLine2'],
                    'streetNumber' => $inputs['streetNumber'],
                ]
            ],

            'items' => $items
        ];



        if (empty($reqdata['interest']['percentage'])) {
            unset($reqdata['interest']);
        }
        if (empty($reqdata['customer']['address']['addressLine2'])) {
            unset($reqdata['customer']['address']['addressLine2']);
        }
        if (empty($reqdata['instructionsMsg'])) {
            unset($reqdata['instructionsMsg']);
        }

        $response = Http::timeout(10)->withBasicAuth($easypag_user, $easypag_pass)
            ->post($easypag_api . 'invoices', $reqdata);

        if ($response->failed()) {
            if ($response->serverError()) {
                return redirect()->route('index')->withErrors(['error' => 'Ocorreu uma falha com o servidor Easypag.']);
            } else if ($response->clientError()) {
                return \Illuminate\Support\Facades\Redirect::back()
                ->withInput()
                ->withErrors(['error' => 'Não foi possível gerar a cobrança. Por favor, verifique a validade dos dados.']);
                return;
            }
        } else {
            $respData = $response->json();
            $toSave = [
                'invoice_id' => $respData['id'],
                'due_date' => $respData['dueDate'],
                'amount' => $respData['amount'],
                'name' => $respData['customer']['name'],
                'doc' => $respData['customer']['doc']['number'],
                'invoice_url' => $respData['invoiceUrl']
            ];
            Charge::create($toSave);
            return redirect()->route('index')->with(['success' => 'Cobrança gerada com sucesso.']);
        }
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Charge  $charge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id');

        $charge = Charge::find($id);

        $easypag_api = env('EASYPAG_URL');
        $easypag_user = env('EASYPAG_USER');
        $easypag_pass = env('EASYPAG_PASSWORD');

        $response = Http::withBasicAuth($easypag_user, $easypag_pass)
        ->post($easypag_api . 'invoices/'. $charge->invoice_id . '/cancel');

        $charge->delete();

        return redirect()->route('index');
    }
}
