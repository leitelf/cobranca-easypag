# Cobrança EasyPag

Desafio proposto em processo Seletivo WillFly. Aplicação com padrão MVC, utilizando última versão estável do Laravel, para consumir API do ambiente sandbox da plataforma de cobranças bancárias EasyPag.

Uma demonstração está disponível no link http://limitless-tundra-73691.herokuapp.com/

# Implantação

Para garantir acesso a API Easypag, deve-se adquirir credencias para o serviço https://docs.easypag.com.br/#introducao

Deve-se garantir que as variáveis de ambiente sejam definidas, seguindo os exemplos do arquivo 
`.env.example`. Podem ser configuradas no ambiente, ou escritas em um arquivo `.env`. É importante definir as variáveis do banco de dados a se utilizar e credenciais da API Easypag.

Para implantação do sistema é necessário que o servidor atenda aos seguintes requisitos do PHP e extensões:
    - PHP >= 7.3
    - BCMath PHP
    - Ctype PHP
    - Fileinfo PHP
    - JSON PHP
    - Mbstring PHP
    - OpenSSL
    - PDO PHP
    - Tokenizer PHP
    - XML PHP

É possível criar instâncias de servidores Nginx seguindo a documentação oficial do framework Laravel https://laravel.com/docs/8.x/deployment#nginx

Para implantação facilitada em serviços de nuvens, aconselhado utilizar https://forge.laravel.com/, ou plataformas com configurações pré-definidas, como heroku https://devcenter.heroku.com/articles/getting-started-with-laravel.

Para criação de banco de dados, utilizar o comando `php artisan migrate`. Variáveis de ambiente referente ao banco de dados devem estar definidas para isso.

