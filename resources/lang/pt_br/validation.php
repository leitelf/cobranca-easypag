<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'required' => ':attribute é obrigatório.',
    'date' => ':attribute deve ser uma data válida.',
    'after_or_equal' => ':attribute deve ser igual ou depois à hoje.',
    'email' => ':attribute deve ser um endereço de email válido.',
    'digits' => ':attribute deve conter :digits dígitos.',
    'digits_between' => ':attribute deve conter entre :min :max dígitos.',
    'between' => ':attribute deve estar entre :min e :max.',
    'in' => ':attribute deve ser uma opção válida.',
    'min' => 'Deve existir pelo menos :min :attribute.',
    'integer' => ':attribute deve ser um número inteiro.',
    'numeric' => ':attribute deve ser um valor numérico.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'dueDate' => 'Data de Vencimento',
        'email' => 'Email',
        'phoneNumber' => 'Número de Telefone',
        'docNumber' => 'Número de Documento',
        'interest_percentage' => 'Juros (% a.m.)',
        'cep' => 'CEP',
        'uf' => 'UF',
        'area' => 'Bairro',
        'addressLine1' => 'Logradouro',
        'addressLine2' => 'Complemento',
        'item_description' => 'Descrição de Itens',
        'item_description.*' => 'Descrição de Item',
        'item_quantity' => 'Quantidade de Itens',
        'item_quantity.*' => 'Quantidade de Item',
        'item_price' => 'Preço de itens',
        'item_price.*' => 'Preço de Item'
    ],

];
