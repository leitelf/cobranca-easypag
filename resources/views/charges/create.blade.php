@extends('charges.layout')

@section('content')
<div class="row">
    <div class="col my-5">
        <h2>Adicionar Cobrança</h2>
    </div>
</div>
<div class="row">
    <div class="col">
        
    @if (count($errors) > 0)
    <ul class="alert alert-danger pl-5">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li> 
        @endforeach
    </ul>
    @endif

        <form action="{{ route('store') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col">
                    <h3>Identificação do cliente</h3>
                    <div class="form-group">
                        <label for="name">Nome/Razão Social</label>
                        <input type="text" name="name" class="form-control" required
                        placeholder="Ex.: João Silva ou Nome Jurídico LTDA">
                    </div>
                    <div class="form-group">
                        <label for="docNumber">CPF/CNPJ</label>
                        <input type="text" name="docNumber" class="form-control" 
                        pattern="([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})"
                        placeholder="Ex.: 000.000.000-00 ou 00000000/0000-00"
                        required>

                    </div>
                    <div class="form-group">
                        <label for="phoneNumber">Telefone</label>
                        <input type="text" name="phoneNumber" class="form-control"
                        pattern="\d*"
                        maxlength="13"
                        minlength="12"
                        placeholder="Ex.: 5585999999"
                        required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" 
                        placeholder="Ex.: exemplo@email.com"
                        required>
                    </div>
                    <hr>
                    <h3>Endereço</h3>
                    <div class="form-group">
                        <label for="cep">CEP</label>
                        <input type="text" name="cep" class="form-control" 
                        placeholder="Ex.: 00000000",
                        pattern="[0-9]{8}"
                        required>
                    </div>
                    <div class="form-group">
                        <label for="uf">UF</label>
                        <select name="uf" class="form-control" required>
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AP">Amapá</option>
                            <option value="AM">Amazonas</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PR">Paraná</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SP">São Paulo</option>
                            <option value="SE">Sergipe</option>
                            <option value="TO">Tocantins</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="city">Cidade</label>
                        <input type="text" name="city" class="form-control" 
                        placeholder="Ex.: São Paulo"
                        required>
                    </div>
                    <div class="form-group">
                        <label for="area">Bairro</label>
                        <input type="text" name="area" class="form-control" 
                        placeholder="Ex.: Tapuapé"
                        required>
                    </div>
                    <div class="form-group">
                        <label for="addressLine1">Logradouro</label>
                        <input type="text" name="addressLine1" class="form-control"
                        placeholder="Rua Lourenço Correa"
                        required>
                    </div>
                    <div class="form-group">
                        <label for="streetNumber">Número</label>
                        <input type="text" name="streetNumber" class="form-control"
                        placeholder="Ex.: 470"
                        required>
                    </div>
                    <div class="form-group">
                        <label for="addressLine2">Complemento</label>
                        <input type="text" name="addressLine2" class="form-control"
                        placeholder="Ex.: Apto 201">
                    </div>
                    <hr>
                    <h3>Informações de cobrança</h3>
                    <div class="form-group">
                        <label for="dueDate">Data de vencimento</label>
                        <input type="date" name="dueDate" class="form-control" 
                        placeholder="Ex.: 30/11/2021"
                        required>
                    </div>
                    <div class="form-group">
                        <label for="interest_percentage">Juros (% a.m.)</label>
                        <input type="number" max="100" min="1" name="interest_percentage" class="form-control"
                        placeholder="Ex.: 50"
                        
                        >
                    </div>
                    <div class="form-group">
                        <label for="instructionsMsg">Instruções do boleto</label>
                        <input type="text" name="instructionsMsg" class="form-control"
                        >
                    </div>
                    <hr>
                    <h3>Itens de cobrança</h3>
                    <div id="item-wrapper">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="item_description[0]">Descrição de item</label>
                                    <input type="text" name="item_description[0]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="item_quantity[0]">Quantidade</label>
                                    <input type="number" min="1" name="item_quantity[0]" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="item_price[0]">Preço</label>
                                    <input type="number" min="0.01" name="item_price[0]" class="form-control" 
                                    step="0.01"
                                    placeholder="Ex.: 10,50"
                                    pattern="[0-9]?[0-9]?(\,[0-9][0-9]?)?"
                                    required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="float-right">
                        <button type="button" onClick="addItem()" class="btn btn-secondary">Adicionar item</button>
                    </div>
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </form>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('js/item.js') }}"></script>
@endsection