@extends('charges.layout')

@section('content')
<div class="row">
    <div class="col my-5">
        <h2>Cobranças</h2>
    </div>
</div>
<div class="row">
    <div class="col text-right my-2">
    <a class="btn btn-primary" href="{{ route('create') }}" role="button">Criar Cobrança</a>
    </div>
</div>
<div class="row">
    <div class="col">
        @if (session('success'))
        <ul class="alert alert-success pl-5">
            <li>{{ session('success') }}</li>
        </ul>
        @endif
        @if (count($errors) > 0)
        <ul class="alert alert-danger pl-5">
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li> 
            @endforeach
        </ul>
        @endif
        <table class="table">
            <thead>
                <tr>
                <th scope="col">Nome/Razão Social</th>
                <th scope="col">CPF/CNPJ</th>
                <th scope="col">Valor</th>
                <th scope="col">Vencimento</th>
                <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($charges as $charge)
                <tr>
                    <td>{{ $charge->name }}</td>
                    <td>{{ $charge->doc }}</td>
                    <td>{{ $charge->amount / 100.0 }}</td>
                    <td>{{ $charge->due_date }}</td>
                    <td>
                        <form action="{{ route('destroy', [ 'id'=> $charge->id ]) }}" method="POST">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">Cancelar</button>
                            <a type="button" href="{{ $charge->invoice_url }}" target="_blank" class="btn btn-primary">Visualizar</a>
                        </form>
                        
                    </td>
                    
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $charges->links() }}
    </div>
</div>
@endsection