var itemCount = 1;

function addItem() {
    fetch('html/item.html').then(data => data.text()).then(item => {
        nitem = item.replaceAll(':id:', itemCount.toString());
        $('#item-wrapper').append(nitem);
        itemCount++;
    });
}

function removeItem(id) {
    $('#'+id).remove();
    itemCount--;
}
